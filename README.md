mpi4py-fft
==========

**WARNING**: This repository has been archived.

**WARNING**: All mpi4py-fft development has moved to [GitHub](https://github.com/mpi4py/mpi4py-fft).

This repository clone at Bitbucket contains mpi4py-fft commits up to **Apr 29, 2021**.

To update your local repository to use the new location, execute

```
git remote set-url origin git@github.com:mpi4py/mpi4py-fft.git
```

or

```
git remote set-url origin https://github.com/mpi4py/mpi4py-fft.git
```
